# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Install https://www.arduino.cc/download_handler.php?f=/arduino-1.6.8-windows.exe
* copy folder Adafruit_CC3000 (you can find it in repo) to C:\Program Files (x86)\Arduino\libraries
* Install drivers\CH341SER_WIN\CH341SER\SETUP.EXE
* Now double click epat-arduino.ino to launch arduino ide
* Attach board using USB
* Select proper COM port from TOOLS-> PORT for debugging.
* Use CTRL + U to upload code to board